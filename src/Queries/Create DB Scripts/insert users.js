// Users of System
db.users.insert({
    _id: 1,
    name: "Sumanta Sarkar",
    password: "1234",
    username: "sumanta.sr@gmail.com"
});
db.users.insert({
    _id: 2,
    name: "Nikhil K",
    password: "12345",
    username: "nikhil@gmail.com"
});


// Counters
db.counters.insert({
    _id: "vendorid",
    seq: 0
});
db.counters.insert({
    _id: "userid",
    seq: 0
});
db.counters.insert({
    _id: "scheduleid",
    seq: 0
});
db.counters.insert({
    _id: "documentid",
    seq: 0
});

// Sales Data
db.sales.insert({
    _id: 1,
    name: "Sumanta S"
});
db.sales.insert({
    _id: 2,
    name: "Bibek M"
});
db.sales.insert({
    _id: 3,
    name: "Nikhil K"
});


// Update Documents
db.getCollection('documents').find({ dateUpdated: null });

db.getCollection("documents").update({
    dateUpdated: null
}, {
    $set: {
        dateUpdated: new Date()
    }
}, false, true);