db.vendors.find().forEach(
    function (elem) {
        var date = new Date();
        db.vendors.update(
            {
                _id: elem._id
            },
            {
                $set: {
                    dateUpdated: date
                }
            }
        );
    }
);