﻿angular.module('trp').controller("addAgentCtrl", function($scope, agentService, $state, $stateParams) {

    $scope.addAgent = function() {
        agentService.addAgent($scope.agent)
            .then(function(result) {
                toastr.success('', 'Agent was added successfully.');
                $state.go("searchagents");
            });
    };


});