﻿angular.module('trp').controller("agentCtrl", function ($scope, agentService, $state, $stateParams) {
    getAgent();

    $scope.isNewAgent = $stateParams.agentId == undefined;

	function getAgent() {
		$scope.agent = {
			companyName: 'Southall Travel',
			companyAddress: 'Southall Travel LTD, Palmoak House, 19 S Rd, Southall, Middlesex, UB1 1SU, United Kingdom',
			agentName: 'Tom Donald',
			agentPhone: '020 8813 8418',
			agentEmail: 'email@southalltravel.co.uk'
		};

		var agentId = $stateParams.agentId;
		if (agentId) {
			$scope.agent = agentService.getAgent(agentId).then(function (result) {
				$scope.agent = result.data;
			});
		}
	}

	$scope.addAgent = function () {
		var agentId = $stateParams.agentId;
		if (agentId) {
			agentService.updateAgent($scope.agent)
				.then(function (result) {
					toastr.success('', 'Agent was updated successfully.');
					$state.go("searchagents");
				});
		}
		else {
			agentService.addAgent($scope.agent)
				.then(function (result) {
					toastr.success('', 'Agent was added successfully.');
					$state.go("searchagents");
				});
		}
	};

	$scope.deleteAgent = function() {
		var agentId = $stateParams.agentId;
		agentService.deleteAgent(agentId).then(function () {
			toastr.success("Agent was deleted successfully.");
			$state.go("searchagents");
	    });
	}
});