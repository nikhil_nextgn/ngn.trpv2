﻿angular.module('trp').controller("communicationCtrl",
    function ($scope, agentService, $state, $stateParams, $timeout) {

        var option = $stateParams.option;
        var agentId = $stateParams.agentId;
        $scope.schedule = {};
        $scope.schedulePopUp = function () {
            $("#myModal").modal();
        }

        function populateSchedule() {
            agentService.getVendorScedules(agentId).then(function (result) {
                $scope.schedules = result.data;
            });
        }

        populateSchedule();

        $scope.saveSchedule = function () {
            agentService.addSchedule($scope.schedule, agentId).then(function(){
                $("#myModal").modal('hide');
                populateSchedule();
            });
        }

        if (option == "newschedule") {
            $scope.schedulePopUp();
        }

        agentService.getSales().then(function (result) {
            $scope.salesPersons = result.data;
            $scope.schedule.salesPerson = $scope.salesPersons[0];
        });


    });