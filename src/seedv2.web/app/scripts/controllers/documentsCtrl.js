﻿angular.module('trp').controller("documentsCtrl", function ($scope, $upload, agentService, $state, $stateParams, $timeout) {
    var option = $stateParams.option;
    var agentId = parseInt($stateParams.agentId);
    $scope.document = {};
    $scope.document.vendorId = agentId;

    $scope.types = ["Legal", "NATA"];
    $scope.document.type = $scope.types[0];

    $scope.statuses = ["New", "Approved"];
    $scope.document.status = $scope.statuses[0];

    $scope.uploadPopUp = function () {
        $scope.files = [];
        //$("#document-modal").modal('show');
        $("#document-modal").modal({
            keyboard: true, show: true
        });
    }

    $scope.saveDocument = function () {

        // do something
        // return;
        // save the document first
        // on promise success do the following

        if ($scope.files && $scope.files.length > 0) {
            console.log($scope.files.length);
            var successCount = 0;

            for (var i = 0; i < $scope.files.length; i++) {
                var file = $scope.files[i];
                $scope.files[i].index = i;
                $scope.files[i].progress = 0;
                $scope.files[i].status = 0;

                console.log($scope.files[i].name);

                $scope.upload = $upload.upload({
                    url: appsettings.apiUrl + 'upload',
                    data: $scope.document,
                    file: file
                }).progress(function (evt) {
                    $scope.files[evt.config.file.index].progress = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + ' is uploaded successfully. Response: ' + data);
                    $scope.files[config.file.index].status = 9;
                    successCount = successCount + 1;
                
                    if (successCount == $scope.files.length) {
                        //getClinicImages();
                        $timeout(function() {
                            $scope.files = [];
                            $("#document-modal").modal('hide');
                            loadDocuments();
                        }, 1000);
                    }
                
                });
            }
        }

        //$("#document-modal").modal('hide');
    }

    if (option == "upload") {
        $scope.uploadPopUp();
    }

    function loadDocuments() {
        agentService.getDocuments(agentId).then(function (result) {
            $scope.documents = result.data;
        });
    }

    loadDocuments();

    $scope.deleteDocument = function(id){
        agentService.deleteVendorDocument(id);
        loadDocuments();
    }

});