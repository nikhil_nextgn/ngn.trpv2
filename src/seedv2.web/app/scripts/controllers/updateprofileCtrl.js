﻿angular.module('trp').controller("updateprofileCtrl",
    function($scope, agentService, $state, $stateParams) {

        console.log($stateParams.agentId);

        var vendorId = $stateParams.agentId;

        agentService.getAgent(vendorId).then(function(result) {
            $scope.agent = result.data;
            if (!result.data){
                toastr.success('Vendor ' + vendorId + ' was not found');
                $state.go("searchagents");
            }
        });

        $scope.updateAgent = function() {
            console.log($scope.agent);
            agentService.updateAgent($scope.agent)
                .then(function(result) {
                    toastr.success('Vendor was updated successfully.');
                }, function(err){
                    console.log(err);
                });
        };

        $scope.deleteAgent = function() {
            agentService.deleteAgent($scope.agent).then(function(result) {
                toastr.success('Vendor was deleted successfully.');
                $state.go("searchagents");
            });
        }


    });