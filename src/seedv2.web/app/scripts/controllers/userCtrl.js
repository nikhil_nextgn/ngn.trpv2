﻿angular.module("trp").controller("userCtrl", function($scope, userService) {
    $scope.user = {
        name : "Nikhil Kaluskar",
        email: "nikhil@nextgn.co.uk",
        userName : "nikhil.kaluskar",
        password : "Passw0rd",
    };
    $scope.addUser = function () {
        userService.addUser($scope.user).then(function(result) {
            toastr.success("", 'User added successfully');
            },
        function(err) {
            toastr.error("", "An error occured, please try again");
            console.log(err);
        });
    };
})