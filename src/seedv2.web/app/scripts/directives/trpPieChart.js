angular.module("trp").directive("trpPieChart", function(){
	return {
		restrict : "E",
		templateUrl : "/views/directives/trpPieChart.html",
		replace: true,
		scope : {
			config : '=',
		},
		link : function(scope, el, atts){
			var elem = $(el[0]).find("#c1")[0];
			$(elem).attr("data-percent", scope.config.percentage);
			console.log(scope);
		},
	}
})