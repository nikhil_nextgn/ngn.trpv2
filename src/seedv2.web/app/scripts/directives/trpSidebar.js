﻿angular.module("trp").directive("trpSidebar", function ($state) {
    return {
        templateUrl: "views/directives/trpSidebar.html",
        link: function(scope, element, attr) {
            scope.isAgentPages = function() {
                if ($state.includes('addagent') || 
                    $state.includes('agent.communication') ||
                    $state.includes('agent.documents') ||
                    $state.includes('searchagents')
                    ) {
                    return true;
                }

                return false;
            }
        }
    }
});