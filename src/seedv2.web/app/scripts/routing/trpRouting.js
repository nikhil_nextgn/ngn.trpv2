﻿angular.module("trp").config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/dashboard");
    $stateProvider
    .state('dashboard', {
        url: "/dashboard",
        templateUrl: "views/dashboard.html",
        controller: "dashboardCtrl",
    })
    .state('addagent', {
        url: "/addagent",
        templateUrl: "views/agent/addagent.html",
        controller: "addAgentCtrl",
    })
    .state('searchagents', {
        url: "/searchagents",
        templateUrl: "views/agent/searchagents.html",
        controller: "agentListCtrl",
    })
    .state('editagent', {
        url: "/agents/:agentId",
        templateUrl: "views/add-agent.html",
        controller: "agentCtrl",
    })
    .state('agent', {
        url: "/agent/:agentId",
        abstract: true,
        templateUrl: "views/agent/agent.html"
    })
    .state('agent.updateprofile', {
        url: "/updateprofile",
        templateUrl: "views/agent/update-profile.html",
        controller: "updateprofileCtrl",
    })
    .state('agent.communication', {
        url: "/communication/:option",
        templateUrl: "views/agent/communication.html",
        controller: "communicationCtrl",
    })
    .state('agent.documents', {
        url: "/documents/:option",
        templateUrl: "views/agent/documents.html",
        controller: "documentsCtrl"
    })
    .state('agent.history', {
        url: "/history",
        templateUrl : "views/agent/history.html"
    })
        .state('adminLoginTab',{
            url:"/adminLoginTab/:Name",
            templateUrl:"views/login.html",
            controller:function($scope, $stateParams) {
                $scope.Name = $stateParams.Name;
                alert('2');
                $scope.login = function(){
                    alert('1');
                };
            }
        })
        .state('agentLoginTab',{
            url:"/agentLoginTab/:Name",
            templateUrl:"views/login.html",
            controller:"loginCtrl"
        })
        .state('legalAdvisorTab',{
            url:"/legalAdvisorTab/:Name",
            templateUrl:"views/login.html",
            controller:"loginCtrl"
        })
        .state('salesLoginTab',{
            url:"/salesLoginTab/:Name",
            templateUrl:"views/login.html",
            controller:"loginCtrl"
        });

    $locationProvider.html5Mode(false);
})
.run(function ($rootScope, $state) {
    $rootScope.$state = $state;
});