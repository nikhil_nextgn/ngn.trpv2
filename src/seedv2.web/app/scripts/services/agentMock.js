﻿"use strict";

angular.module('agentMock').run(function ($httpBackend) {
    var agents = [
        {
            id: 1,
            companyName: 'Southall Travel',
            companyAddress: 'Southall Travel LTD, Palmoak House, 19 S Rd, Southall, Middlesex, UB1 1SU, United Kingdom',
            agentName: 'Tom Donald',
            agentPhone: '020 8813 8418',
            agentEmail: 'email@southalltravel.co.uk'
        },
        {
            id: 2,
            companyName: 'yeti travels',
            companyAddress: 'Durbar Marg, P.O. Box 76, Kathmandu, Nepal',
            agentName: 'Baichung Vikas',
            agentPhone: '977-1-4221234, 422474',
            agentEmail: 'contact@yetitravels.com.np'
        },
        {
            id: 3,
            companyName: 'Thamel Travels and Tours Pvt. Ltd.',
            companyAddress: 'P. O. Box: 10988,Thamel, Kathmandu, Nepal',
            agentName: 'Mr Thamel',
            agentPhone: '+977 1 4253029, +977 9841697870',
            agentEmail: 'info@thameltravel.com'
        },
        {
            id: 4,
            companyName: 'Intel Roshi Tour and Travels Pvt. Ltd.',
            companyAddress: 'P.O.Box:13538, Thamel, Kathamndu, Nepal.',
            agentName: 'Sujan Roshi',
            agentPhone: '+977-1-4701732, 4701274, 4700157, +977-1-9851039629',
            agentEmail: ' info@nepaltouragency.com'
        }
    ];

    var agentsUrl = appsettings.apiUrl + "agent";

    //Get all
    $httpBackend.whenGET(agentsUrl).respond(agents);

    var editingRegex = new RegExp(agentsUrl + "/[0-9][0-9]*", '');

    //Get By id
    $httpBackend.whenGET(editingRegex).respond(function (method, url, data) {
        var agent = { "id": 0 };
        var parameters = url.split('/');
        var length = parameters.length;
        var id = parameters[length - 1];

        if (id > 0) {
            for (var i = 0; i < agents.length; i++) {
                if (agents[i].id == id) {
                    agent = agents[i];
                    break;
                }
            };
        }
        return [200, agent, {}];
    });

    //POST 
    $httpBackend.whenPOST(agentsUrl).respond(function (method, url, data) {
        var agent = angular.fromJson(data);

        if (!agent.id) {
            // new agent Id
            agent.id = agents[agents.length - 1].id + 1;
            agents.push(agent);
        }
        return [200, agent, {}];
    });

    //PUT 
    $httpBackend.whenPUT(agentsUrl).respond(function (method, url, data) {
        var agent = angular.fromJson(data);

        if (agent.id) {
            // Updated agent
            for (var i = 0; i < agents.length; i++) {
                if (agents[i].id == agent.id) {
                    agents[i] = agent;
                    break;
                }
            };
        }
        return [200, agent, {}];
    });

    //DELETE 
    var deleteUrl = new RegExp(agentsUrl + "/[0-9][0-9]*", '');
    $httpBackend.whenDELETE(deleteUrl).respond(function (method, url) {

        var parameters = url.split('/');
        var length = parameters.length;
        var id = parameters[length - 1];

        if (id > 0) {
            for (var i = 0; i < agents.length; i++) {
                if (agents[i].id == id) {
                    agents.pop(agents[i]);
                    break;
                }
            };
        }
        return [200, null, {}];
    });

    $httpBackend.whenGET(/views/).passThrough();
});
