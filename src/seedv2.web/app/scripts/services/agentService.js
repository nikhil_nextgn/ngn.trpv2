﻿angular.module('trp').service('agentService', function($http) {
    var addAgent = function(agent) {
        return $http.post(appsettings.apiUrl + 'vendor', agent);
    };

    var deleteAgent = function(vendor) {
        return $http.delete(appsettings.apiUrl + 'vendor/' + vendor._id);
    };

    var getAllAgents = function() {
        return $http.get(appsettings.apiUrl + 'vendors');
    };

    var getAgent = function(vendorId) {
        return $http.get(appsettings.apiUrl + 'vendor/' + vendorId);
    };

    var updateAgent = function(vendor) {
        return $http.put(appsettings.apiUrl + 'vendor', vendor);
    };

    return {
        addAgent: addAgent,
        updateAgent: updateAgent,
        deleteAgent: deleteAgent,
        getAllAgents: getAllAgents,
        getAgent: getAgent,
        searchVendors: function(searchStr) {
            return $http.get(appsettings.apiUrl + 'vendors?searchStr=' + searchStr);
        },
        getSales: function() {
            return $http.get(appsettings.apiUrl + 'sales');
        },
        addSchedule: function(schedule, vendorId) {
            return $http.post(appsettings.apiUrl + 'schedule?vendorId=' + vendorId, schedule);
        },
        getVendorScedules: function(vendorId){
            return $http.get(appsettings.apiUrl + 'vendorSchedules/' + vendorId);
        },
        getDocuments: function(vendorId){
            return $http.get(appsettings.apiUrl + 'vendorDocuments/' + vendorId);
        },
        deleteVendorDocument: function(id){
            return $http.delete(appsettings.apiUrl + 'vendorDocument/' + id);
        }
    };
});