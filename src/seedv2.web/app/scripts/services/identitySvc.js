﻿angular.module('lld').factory('identitySvc', function(cookieSvc) {
   
    function getIdentity() {
        var identityString = cookieSvc.getCookie("Identity");
        if (identityString) {
            return jQuery.parseJSON(identityString);
        }
        return undefined;
    };

    return {
       identity: getIdentity(),
       isLoggedIn: function() {
           var identityLocal = getIdentity();
           if (identityLocal && identityLocal.UserId) {
               return true;
           }
           return false;
       }
    };
   
});