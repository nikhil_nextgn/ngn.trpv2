﻿angular.module("trp").service("userService", function($http) {
    var addUser = function(user) { return $http.post(appsettings.apiUrl + "adduser", user); };

    return {
        addUser: addUser
    }
})