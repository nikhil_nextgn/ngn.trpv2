﻿var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');

module.exports = {}
var auth = module.exports;

var UserModel = require("../models/user.js");
var hasher = require("../auth/hasher.js");

var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

auth.ensureAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        if (config.byPassAuth) {
            next();
        } else {
            res.redirect("/login");
        }
    }
};

auth.ensureApiAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        if (config.byPassAuth) {
            next();
        } else {
            res.send(401, "Not authorized");
        }
    }
};

auth.init = function(app) {

    // setup passport authentication
    passport.use(new LocalStrategy(function(username, password, next) {
        MongoClient.connect(config.mongoUrl, function(err, db) {
            db.collection('users').findOne({
                username: username,
                password: password
            }, function(err, user) {
                db.close();
                if (!user) {
                    return next(null, false);
                }
                return next(null, user);
            });
        });
    }));

    passport.serializeUser(function(user, next) {
        next(null, user);
    });
    passport.deserializeUser(function(user, next) {
        next(null, user);
        // UserModel.finnext(user.userName, function(err, user1) {
        //     if (err || !user1) {
        //         next(null, false, {
        //             message: "Could not find user"
        //         });
        //     } else {
        //         next(null, user1);
        //     }
        // });
    });
    app.use(passport.initialize());
    app.use(passport.session());

    app.get("/login", function(req, res) {
        res.render("login", {
            title: "Login to The Board",
            message: "loginError"
        });
    });

    app.post("/login", function(req, res, next) {
        var authFunction = passport.authenticate("local", function(err, user, info) {
            if (err) {
                next(err);
            } else {
                req.logIn(user, function(err) {
                    if (err) {
                        res.redirect("/login");
                        next(err);
                    } else {
                        res.redirect("/");
                    }
                });
            }
        });
        authFunction(req, res, next);
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.post("/api/adduser",
        auth.ensureAuthenticated,
        function(req, res) {
            var salt = hasher.createSalt();
            var passwordHash = hasher.computeHash(req.body.password, salt);

            var user = UserModel({
                name: req.body.name,
                email: req.body.email,
                userName: req.body.userName,
                passwordHash: passwordHash,
                salt: salt
            });

            user.save(function(err, result) {
                if (err) {
                    res.status(500).send(err);
                }

                res.status(200).send({
                    "message": "user created successfully"
                });
            });
        });
};