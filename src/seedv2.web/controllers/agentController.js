﻿module.exports = {};
var agentController = module.exports;
var AgentModel = require("../models/agent.js");
var auth = require("../auth");

agentController.init = function(app) {

    app.get("/api/agents",
        auth.ensureApiAuthenticated,
        function(req, res) {
            var data = [{
                name: "sumanta",
                age: 32
            }, {
                name: "Nikhil K",
                age: 30
            }];
            res.send(data);
        });

    app.get("/api/agent/:agentId",
        auth.ensureApiAuthenticated,
        function(req, res) {
            var query = AgentModel.findOne({
                "_id": req.params.agentId
            });
            query.exec(function(err, results) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.send(results);
                }
            });
        });

    app.post("/api/agent",
        auth.ensureApiAuthenticated,
        function(req, res) {
            var agent = new AgentModel({
                agentName: req.body.agentName,
                agentPhone: req.body.agentPhone,
                agentEmail: req.body.agentEmail,
                companyName: req.body.companyName,
                companyAddress: req.body.companyAddress,
            });

            agent.save(function(err, results) {
                if (err) {
                    res.send(err);
                } else {
                    res.status(204).send(results);
                }
            });
        });

    app.put("/api/agent/:agentId",
        auth.ensureApiAuthenticated,
        function(req, res) {
            var agent = AgentModel.findById(req.params.agentId, function(err, agent) {

                agent.agentName = req.body.agentName;
                agent.agentPhone = req.body.agentPhone;
                agent.agentEmail = req.body.agentEmail;
                agent.companyName = req.body.companyName;
                agent.companyAddress = req.body.companyAddress;

                agent.save(function(ex, results) {
                    if (err) {
                        res.statu(500).send(ex);
                    }

                    res.status(204).send(results);
                });
            });
        });

    app.delete("/api/agent/:agentId", auth.ensureApiAuthenticated,
        function(req, res) {
            var query = AgentModel.find({
                "_id": req.params.agentId
            }).remove();

            query.exec(function(err, results) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(204).send("Resource deleted successsfully.");
                }
            });
        });
}

//module.exports = {};
//var agentController = module.exports;
//agentController.init = function (app) {
//    app.post("/api/agent", function (req, res) {
//        res.set("Content_Type", "application-json");
//        res.send({ "id": "100" });
//    });
//}

//(function (agentController) {
//    agentController.init = function (app) {
//        app.get("/api/agent", function (req, res) {
//            res.set("Content-Type", "application/json");
//            res.send( {"id" : 1} );
//        });
//    };
//})(module.exports);