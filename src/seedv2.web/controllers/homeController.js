(function(homeController) {
    var auth = require("../auth");
    homeController.init = function (app) {
        app.get("/", auth.ensureAuthenticated, function (req, res) {
            //var mongoose = require("mongoose");
            //mongoose.connect("mongodb://dev.user1:Passw0rd@ds056727.mongolab.com:056727/thrillic");
            //var db = mongoose.connection;
            
            //db.on('error', console.error.bind(console, 'connection error'));
            
            //db.once('open', function () {
            //    var title = "Connected to Mongo";
            //    res.render("index", { title : title });
            //}, function(ex) {
            //    console.log(ex);
            //    var title = "Hello vash from Nodemon";
            //    res.render("index", { title : title });
            //});
            res.render("home", { user: req.user });
        });

        app.get("/layout/simple", auth.ensureAuthenticated,
            function(req, res) {
                res.render("index", { title: "Hello vash from Nodemon" } );
            });
    };
})(module.exports);
