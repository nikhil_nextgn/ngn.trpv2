﻿module.exports.init = function(app) {
	require("./homeController").init(app);
    require("./agentController").init(app);
    require("./vendorController").init(app);
    require("./uploadController").init(app);
};

