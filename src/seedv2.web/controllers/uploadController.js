﻿var auth = require("../auth");
var dataService = require("../services/documentService.js");
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var multer = require('multer');
var fs = require("fs");

module.exports.init = function (app) {
    app.post("/api/upload",
        [multer({dest: './app/uploads/'}),
            function (req, res) {
                console.log(req.body); // form fields
                dataService.addDocument(req.body, req.files, function (err, result) {
                    res.sendStatus(204);
                });
            }]);


    //app.post("/api/upload",
    //    [multer({
    //        inMemory: true,
    //        onFileUploadComplete: function (file, req, res) {
    //            console.log("upload compeltyed");
    //        }
    //    }),
    //        function (req, res) {
    //            res.sendStatus(204);
    //
    //            //console.log(req.files.file.name);
    //            //console.log(req.files.file);
    //
    //            fs.writeFile(
    //                req.files.file.name,
    //                req.files.file.buffer,
    //                function (err) {
    //                if (err) throw err;
    //                console.log('It\'s saved!');
    //            });
    //
    //            console.log("Writing at the END after status is send"); // form fields
    //        }]);

    app.get("/api/upload/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            // Get from Azure Stream
            // res.send (image);
            res.send("Image");
        });
}