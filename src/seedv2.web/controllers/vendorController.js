﻿var auth = require("../auth");
var dataService = require("../services/vendorService.js");
var documentService = require("../services/documentService.js");
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var multer = require('multer');

module.exports.init = function (app) {

    // get Records
    app.get("/api/vendors",
        auth.ensureApiAuthenticated,
        function (req, res) {

            var condition = {};
            var searchStr = req.query.searchStr;
            if (searchStr) {
                console.log(searchStr);
                condition = {
                    agentName: {
                        $regex: new RegExp("^" + searchStr, "i")
                    }
                };
            }

            MongoClient.connect(config.mongoUrl, function (err, db) {
                db.collection("vendors")
                    .find(condition, {})
                    .sort({
                        dateUpdated: -1
                    })
                    .toArray(function (err, result) {
                        db.close();
                        res.send(result);
                    });
            });
        });

    app.get("/api/vendor/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            MongoClient.connect(config.mongoUrl, function (err, db) {
                db.collection("vendors")
                    .findOne({
                        _id: parseInt(req.params.id)
                    }, {}, function (err, doc) {
                        console.log(doc);
                        db.close();
                        res.send(doc);
                    });

            });
        });

    app.get("/api/sales",
        auth.ensureApiAuthenticated,
        function (req, res) {
            MongoClient.connect(config.mongoUrl, function (err, db) {
                db.collection("sales")
                    .find({}, {})
                    .sort({
                        name: 1
                    })
                    .toArray(function (err, result) {
                        db.close();
                        res.send(result);
                    });
            });
        });

    app.get("/api/vendorSchedules/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            MongoClient.connect(config.mongoUrl, function (err, db) {
                db.collection("schedules")
                    .find({
                        "vendor._id": parseInt(req.params.id)
                    }, {})
                    .toArray(function (err, doc) {
                        console.log(doc);
                        db.close();
                        res.send(doc);
                    });
            });
        });

    app.get("/api/vendorDocuments/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            MongoClient.connect(config.mongoUrl, function (err, db) {
                db.collection("documents")
                    .find({
                        "vendorId": parseInt(req.params.id)
                    }, {})
                    .sort({
                        dateUpdated: -1
                    })
                    .toArray(function (err, doc) {
                        console.log(doc);
                        db.close();
                        res.send(doc);
                    });
            });
        });

    // Delete
    app.delete("/api/vendor/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            dataService.deleteVendor(parseInt(req.params.id), function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.sendStatus(204);
                }
            });
        });

    app.delete("/api/vendorDocument/:id",
        auth.ensureApiAuthenticated,
        function (req, res) {
            documentService.deleteDocument(req.params.id, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.sendStatus(204);
                }
            });
        });


    // Create or Insert
    app.post("/api/vendor",
        auth.ensureApiAuthenticated,
        function (req, res) {
            var data = req.body;
            dataService.addVendor(data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.sendStatus(204);
                }
            });
        });

    app.post("/api/schedule",
        auth.ensureApiAuthenticated,
        function (req, res) {
            //res.send("some data");
            var schedule = req.body;
            var vendorId = req.query.vendorId;
            dataService.addSchedule(vendorId, schedule, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.sendStatus(204);
                }
            });
        });

    // Update
    app.put("/api/vendor",
        auth.ensureApiAuthenticated,
        function (req, res) {
            var data = req.body;
            dataService.updateVendor(data, function (err, result) {
                if (err) {
                    console.log("err = " + err);
                    res.send(err);
                } else {
                    console.log("result = " + result);
                    res.sendStatus(204);
                }
            });
        });
}