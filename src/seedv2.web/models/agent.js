﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var agentSchema = new Schema({
    agentName: String,
    agentPhone: String,
    agentEmail: String,
    companyName: String,
    companyAddress: String,
    createdOn: {type : Date, default : Date.now}
});

module.exports = mongoose.model("Agent", agentSchema);