﻿var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name : String,
    email : String,
    userName: String,
    passwordHash : String,
    salt: String,
    createdOn: { type : Date, default : Date.now }
});

module.exports = mongoose.model("User", userSchema);