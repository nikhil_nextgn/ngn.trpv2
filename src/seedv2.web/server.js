﻿var http = require("http");
var bodyParser = require("body-parser");
var express = require("express");
var session = require('express-session');
var cookieParser = require('cookie-parser');
var multer = require('multer');

var app = express();
app.set("view engine", "vash");


app.use(bodyParser.json());
app.use(session({ secret: "Thrillic", resave : false, saveUninitialized : false }));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

var auth = require("./auth");
auth.init(app);

var config = require('./config/config.js');

console.log(config.url);

var controllers = require("./controllers");
controllers.init(app);


// Static should be below Router for performance
// app.use(express.static(__dirname + "/app"));
app.use(express.static("app"));

server = http.createServer(app);
server.listen(81);