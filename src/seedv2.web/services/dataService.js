﻿module.exports.getExternalId = function(db, id, callback) {
    db.collection("counters").findAndModify({
            _id: id
        }, // query
        [], // represents a sort order if multiple matches
        {
            "$inc": {
                "seq": 1
            }
        }, // update statement
        {
            new: true
        }, // options - new to return the modified document
        function(err, doc) {
            console.log(doc);
            callback(doc);
        }
    );
};
