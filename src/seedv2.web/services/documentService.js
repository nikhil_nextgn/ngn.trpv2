var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var dataService = require('../services/dataService.js');
var _ = require('underscore');
var merge = require('merge');

exports.addDocument = function (docProp, fileProp, callback) {
    var data = {};
    data = _.extend(fileProp.file, docProp);
    console.log(data);

    MongoClient.connect(config.mongoUrl, function (err, db) {
        db.collection('vendors').findOne({
                _id: parseInt(docProp.vendorId)
            },
            function (err, vendor) {
                dataService.getExternalId(db, "documentid", function (sequence) {
                    data._id = sequence.seq;
                    data.vendorId = parseInt(data.vendorId);
                    data.vendor = vendor;
                    data.dateUpdated = new Date();
                    db.collection('documents').insert(data, function (err, result) {
                        db.close();
                        callback(err, result);
                    });
                });
            });
    });
};

exports.deleteDocument = function (id, callback) {
    MongoClient.connect(config.mongoUrl, function (err, db) {
        db.collection('documents').remove({_id: parseInt(id)},
            function (err, result) {
                db.close();
                console.log("deleted vendor document " + id);
                callback(err, result)
            });
    });
};