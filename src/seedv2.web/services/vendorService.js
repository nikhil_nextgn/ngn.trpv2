var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config.js');
var dataService = require('../services/dataService.js');

exports.addVendor = function (data, callback) {
    MongoClient.connect(config.mongoUrl, function (err, db) {
        var vendors = db.collection('vendors');
        data.dateUpdated = new Date();
        dataService.getExternalId(db, "vendorid", function (sequence) {
            data._id = sequence.seq;
            vendors.insert(data, function (err, result) {
                console.log("added vendors");
                db.close();
                callback(err, result);
            });
        });
    });
};

exports.addSchedule = function (vendorId, schedule, callback) {
    MongoClient.connect(config.mongoUrl, function (err, db) {
        console.log(vendorId);
        db.collection('vendors').findOne({
                _id: parseInt(vendorId)
            },
            function (err, vendor) {
                console.log(vendor);
                dataService.getExternalId(db, "scheduleid", function (sequence) {
                    schedule._id = sequence.seq;
                    schedule.vendor = vendor;
                    db.collection('schedules').insert(schedule, function (err, result) {
                        console.log("added schedule");
                        db.close();
                        callback(err, result);
                    });
                });
            });
    });
};

exports.updateVendor = function (data, callback) {
    MongoClient.connect(config.mongoUrl, function (err, db) {
        data.dateUpdated = new Date();
        var vendors = db.collection('vendors');
        vendors.update({
                _id: parseInt(data._id)
            },
            data,
            function (err, result) {
                console.log("vendor updated - " + data.externalId);
                db.close();
                callback(err, result);
            });
    });
};

exports.deleteVendor = function (id, callback) {
    MongoClient.connect(config.mongoUrl, function (err, db) {
        var collection = db.collection('vendors');
        collection.remove({
            _id: parseInt(id)
        }, function (err, result) {
            db.close();
            callback(err, result);
        });
    });
};